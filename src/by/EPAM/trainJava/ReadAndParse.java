package by.EPAM.trainJava;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class ReadAndParse {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		String FileName="test.xml";
		try (BufferedReader br = new BufferedReader(new FileReader("d:\\work\\JAVA\\lesson10\\src\\by\\EPAM\\trainJava\\"+FileName)))
		{
		    String sCurrentLine;
		    while ((sCurrentLine = br.readLine()) != null) {
		        sb.append(sCurrentLine);
		    }
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		//System.out.println(sb);

		XmlParse p= new XmlParse(FileName,sb.toString());
		p.parseStart();
		System.out.println(p);
	}

}
